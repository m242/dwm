/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "xos4 Terminus:style=Regular:pixelsize=20" };

/* My colors, and color theme */
static const char col_bg[]		= "#121212";
static const char col_focus[]	= "#EEEEEE";
static const char col_fg[]		= "#707070";
static const char *colors[][3] 	= {
	/*fg, bg, border*/
	[SchemeNorm] = { col_fg, col_bg, col_fg},
	[SchemeSel]	 = { col_bg, col_fg, col_focus},
};

/* tagging */
static const char *tags[] = { "dev", "doc", "soc", "misc" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ NULL,       NULL,       NULL,       0,            False,       -1 },
};

/* layout(s) */
static const float mfact     = 0.60; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "",      	monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

static Key keys[] = {
	/* modifier                     key        function        argument */
	/* Window Actions */
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_j, 		 zoom,           {0} },
	{ MODKEY|ShiftMask,             XK_q,      killclient,     {0} },
	
	/* Tags && layouts */
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} }, // Tiling
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[1]} }, // Monocle
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY,                       XK_F10,    view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_F10,    tag,            {.ui = ~0 } },
  
	/* Monitor Actions */
	{ MODKEY,                       XK_h,      focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_l,      focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_h, 		 tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_l, 		 tagmon,         {.i = +1 } },
	
	/* Else */
	TAGKEYS(                        XK_F1,                      0)
	TAGKEYS(                        XK_F2,                      1)
	TAGKEYS(                        XK_F3,                      2)
	TAGKEYS(                        XK_F4,                      3)
	{ MODKEY|ShiftMask,             XK_e,      quit,           {0} },
};

/* button definitions */
/* Mouse is useless, 1337 h4xx0rz 0nly uzz t3h k33b */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ 0,										0,							0,							0,							{0}},
};
